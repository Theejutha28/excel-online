<!doctype html>
<html lang="th">
    <head>
        <meta charset="utf-8">
        <title>Jumpstart SaaS App & Software Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Bootstrap HTML template and UI kit by Medium Rare">

        @include('includes.head')
        @yield('script')
    </head>
    <body>
        <div class="loader">
          <div class="loading-animation"></div>
        </div>
        
        <div class="wrapper">
            @include('includes.header')
            @yield('content')
        </div>
        
        @include('includes.footer')
    </body>
</html>
