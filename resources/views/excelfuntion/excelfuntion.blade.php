@extends('layouts.default')
@section('content')
	<section class="p-0 border-top border-bottom bg-light row no-gutters">
    <div class="col-lg-5 col-xl-6 order-lg-2">
      <div class="divider divider-side transform-flip-y bg-light d-none d-lg-block"></div>
      <img class="flex-fill" src="{{ asset('img/case-studies/large-1.jpg') }}" alt="blog.1.image">
    </div>
    <div class="col-lg-7 col-xl-6">
      <div class="container min-vh-lg-70 min-vh-xl-80 d-flex align-items-center">
        <div class="row justify-content-center">
          <div class="col col-md-10 col-xl-9 py-4 py-sm-5">
            <div class="my-4 my-md-5 my-lg-0 my-xl-5">
              <img src="{{ asset('img/logos/brand/aven.svg') }}" alt="Aven company logo">
              <h1 class="display-4 mt-4 mt-lg-5">
            &ldquo;We are working at almost twice the capacity&rdquo;
          </h1>
              <p class="lead">
                Volantis vitae unuch sed velit sodales. Sandor imperdiet proin fermentum leo vel Hodor.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    <section>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-11 col-lg-11">
            <div id="faq-accordion">
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-1" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันคำนวณการลงทุน</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-1" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน FV คำนวณเงินในอนาคต
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน PV คำนวณเงินในปัจจุบัน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน NPV/XNPV หามูลค่าเงินสุทธิในปัจจุบัน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IRR/MIRR หาอัตราผลตอบแทนการลงทุน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน Disc หาอัตราส่วนลด
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน RECEIVED หาจำนวนเงินที่จะได้รับ
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน DURATION หาค่าเฉลี่ยตามน้ำหนัก
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน YIELD หาอัตราผลตอบแทน
                    </div>
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-2" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันคำนวณดอกเบี้ยและการผ่อนจ่าย</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-2" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน Rate หาอัตราดอกเบี้ย
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา NPER หาระยะเวลา
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา PMT หาเงินผ่อนที่ชำระใรแต่ละงวด
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา IPMT หาดอกเบี้ยชำระในแต่ละงวด
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา PPMT หาเงินผ่อนที่ชำระใรแต่ละงวด
                    </div>
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-3" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันคำนวณหาค่าเสื่อมราคา</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-3" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา SLN หาค่าเสื่อมราคาแบบเส้นตรง
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา DB หาค่าเสื่อมราคาแบบลดลงแน่นอน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา VDB หาค่าเสื่อมราคาแบบแปรผัน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา DDB หาค่าเสื่อมราคาแบบลดลงสองเท่า
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชันการหา SYD หาค่าเสื่อมราคาแบบนับผลรวมประจำปี
                    </div>
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-4" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันทางตรรกะ</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-4" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน AND และ OR เปรียบเทียบค่า
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน NOT, TRUE และ FALSE ส่งค่าตรรกะ
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IF ตรวจสอบเงื่อนไข
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IFERROR กรณีพบความผิดพลาด
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IFS ตรวจสอบหลายเงื่อนไขพร้อมกัน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน SWITCH ตรวจสอบค่าในนิพจน์
                    </div>
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-4" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันทางตรรกะ</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-4" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน AND และ OR เปรียบเทียบค่า
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน NOT, TRUE และ FALSE ส่งค่าตรรกะ
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IF ตรวจสอบเงื่อนไข
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IFERROR กรณีพบความผิดพลาด
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน IFS ตรวจสอบหลายเงื่อนไขพร้อมกัน
                    </div>
                    <div class="col-md-12 col-lg-12 pb-md-4">
                      ฟังก์ชัน SWITCH ตรวจสอบค่าในนิพจน์
                    </div>
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>

              <div class="card mb-2 mb-md-3">
                <a href="#accordion-5" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันการแปลงตัวเลขและข้อความ</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-5" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-6" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันการจัดรูปแบบข้อความ</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-6" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-7" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันการค้นหาและแทนที่</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-7" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-8" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันเกี่ยวกับวันที่</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-8" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-9" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันคำนวณหาช่วงวันที่แตกต่าง</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-9" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
              <div class="card mb-2 mb-md-3">
                <a href="#accordion-10" data-toggle="collapse" role="button" aria-expanded="false" class="p-3 p-md-4">
                  <div class="d-flex justify-content-between align-items-center">
                    <h5 class="mb-0 mr-2">ฟังก์ชันเกี่ยวกับเวลา</h5>
                    <img src="{{ asset('img/icons/interface/icon-caret-right.svg') }}" alt="Caret Right" class="icon icon-sm">
                  </div>
                </a>
                <div class="collapse" id="accordion-10" data-parent="#faq-accordion">
                  <div class="px-3 px-md-4 pb-3 pb-md-4">
                    
                  </div>
                  <div class="col-md-10 col-lg-4 offset-lg-4 pb-md-4">
                    <a href="#" class="btn btn-lg btn-block btn-outline-primary"> Download</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
   
@stop