@extends('layouts.default')
@section('script')
  <style type="text/css">
    section {
      padding: 50px;
    }
  </style>
@stop
@section('content')
  <div data-overlay class="bg-primary-3 jarallax text-white" data-jarallax data-speed="0.2">
    <img src="{{ asset('img//blog/thumb-1.jpg') }}" alt="Background" class="jarallax-img opacity-30">
    <section class="pb-0">
      <div class="container pb-5">
        <div class="row justify-content-center text-center">
          <div class="col-xl-8 col-lg-10 col-md-11">
            <h1 class="display-3" data-aos="fade-up" data-aos-delay="100">เรียนซ้ำๆ ให้เข้าใจ</h1>
            <p class="lead" data-aos="fade-up" data-aos-delay="200">
              Berspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
            </p>
            <div class="d-flex flex-column flex-sm-row justify-content-center mt-4 mt-md-5" data-aos="fade-up" data-aos-delay="300">
              <a href="#" class="btn btn-primary btn-lg mx-sm-2 my-1 my-sm-0">สมัครเรียนทาง Line</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <h5 class="display-4 text-center" style="margin-bottom: 50px;">ราคาพิเศษ 999</h5>
        </div>
        <div class="col-md-4 mb-4 mb-md-0">
          <iframe width="100%" height="210" src="https://www.youtube.com/embed/EtEBvs8ddPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4 mb-4 mb-md-0">
          <iframe width="100%" height="210" src="https://www.youtube.com/embed/EtEBvs8ddPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4 mb-4 mb-md-0">
          <iframe width="100%" height="210" src="https://www.youtube.com/embed/EtEBvs8ddPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4 mb-4 mb-md-0">
          <iframe width="100%" height="210" src="https://www.youtube.com/embed/EtEBvs8ddPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4 mb-4 mb-md-0">
          <iframe width="100%" height="210" src="https://www.youtube.com/embed/EtEBvs8ddPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4 mb-4 mb-md-0">
          <iframe width="100%" height="210" src="https://www.youtube.com/embed/EtEBvs8ddPM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>
  <section class="bg-light">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
          <div class="card card-body align-items-center shadow">
            <div class="text-center mb-4">
              <h4>Master Excel</h4>
            </div>
            <ul class="p-0">
              <li class="my-3">
                 <h6 class="mb-0 ml-3">2GB Cloud Storage</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">100GB CDN Bandwidth</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">98.88% Uptime Guarantee</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">Personal Account Manager</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">Enterprise SLA</h6>
              </li>
            </ul>
            <a href="#" class="btn btn-lg btn-block btn-outline-primary">Start with Basic</a>
          </div>
        </div>
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="200">
          <div class="card card-body align-items-center shadow">
            <div class="text-center mb-4">
              <h4>Formulas and Functions</h4>
            </div>
            <ul class="p-0">
              <li class="my-3">
                  <h6 class="mb-0 ml-3">20GB Cloud Storage</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">1TB CDN Bandwidth</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">99.95% Uptime Guarantee</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">Personal Account Manager</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">Enterprise SLA</h6>
              </li>
            </ul>
            <a href="#" class="btn btn-lg btn-block btn-primary">Start with Premium</a>
          </div>
        </div>
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="300">
          <div class="card card-body align-items-center shadow">
            <div class="text-center mb-4">
              <h4>Excel Data Visualization</h4>
            </div>
            <ul class="p-0">
              <li class="my-3">
                <h6 class="mb-0 ml-3">Unlimited Storage</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">100TB CDN Bandwidth</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">99.999% Uptime Guarantee</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">Personal Account Manager</h6>
              </li>
              <li class="my-3">
                <h6 class="mb-0 ml-3">Enterprise SLA</h6>
              </li>
            </ul>
            <a href="#" class="btn btn-lg btn-block btn-outline-primary">Start with Pro</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-lg-10 offset-lg-1 my-2 my-md-3 my-lg-0">
          <div class="row">
            <a class="col-md-6 col-lg-6" href="#">
              <img class="rounded img-fluid hover-fade-out" src="{{ asset('img//blog/thumb-1.jpg') }}" alt="blog.2.image" style="height: 200px; width: 100%;">
            </a>
            <div class="col-md-6 col-lg-6">
              <div class="h2 text-muted mt-2">ราคาพิเศษเพียง 999 -.</div>
              <a class="h4" href="#">สมัครผ่านทาง Line</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="p-0">
    <div class="divider divider-top bg-light transform-flip-x"></div>
    <div class="container">
      <div class="row section-title justify-content-center">
        <div class="col-md-9 col-lg-8 col-xl-7">
          <h4 class="display-4 text-center" style="margin-bottom: 60px;">คำถามที่พบบ่อย</h4>
        </div>
        <div class="col-md-10 col-lg-10 my-2 my-md-3 my-lg-0">
          <div class="row">
            <div class="col-md-6 col-lg-6">
              <h5>การสมัครเรียนผ่านแอดมิน (โอนเงินผ่านธนาคาร)</h5>
              <p>1. สมัครเรียนผ่านแอดมินได้ที่ช่องทางต่อไปนี้ Line@: @ocsctutor</br>
                2. หลังจากคุณชำระเงินแล้วแอดมินจะแจ้งรายละเอียดการเข้าใช้งานให้ทราบ</br>
                3. คุณสามารถทำตามขั้นตอนที่ได้รับและเริ่มเรียนได้ทันที</p>
            </div>
            <a class="col-md-6 col-lg-6" href="#">
              <img class="rounded img-fluid hover-fade-out" src="{{ asset('img//blog/thumb-1.jpg') }}" alt="blog.2.image" style="height: 200px; width: 100%;">
            </a>
          </div>
        </div>
      </div>
      
    </div>
    <div class="divider divider-bottom bg-primary-3"></div>
  </section>
    
@stop