@extends('layouts.default')
@section('script')
  <!-- <script src='https://kit.fontawesome.com/a076d05399.js'></script> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@stop
@section('content')
  <section class="pt-5 pb-5">
    <div class="container">
      <div class="row justify-content-end bg-white display-none-dt">
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <i class='fa fa-clock-o' style="font-size: 70px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <!-- <i class='fa fa-clock' style="font-size: 100px;"></i> -->
              <i class="fa fa-money" style="font-size: 70px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <h4 class="mb-0">ระยะเวลาเรียน</h4>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2">30</span>
              <span class="h5 mb-0">ชั่วโมง</span>
            </div>
          </div>
        </div>
        
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center">
            <div class="d-flex justify-content-center mb-1">
              <h4 class="mb-0 text-dark">ราคาเรียน</h4>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2 text-dark">15,000</span>
              <span class="h5 mb-0">.-</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-end bg-white display-none-mb">
        <div class="col-lg-1 py-3 py-md-2 bg-info">
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-white">
            <div class="d-flex mb-1">
              <i class='fa fa-clock-o' style="font-size: 100px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-white">
            <div class="d-flex mb-1">
              <h4 class="mb-0">ระยะเวลาเรียน</h4>
            </div>
            <div class="d-flex align-items-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2">30</span>
              <span class="h5 mb-0">ชั่วโมง</span>
            </div>
          </div>
        </div>
        <div class="col-lg-1 py-3 py-md-2 bg-info">
        </div>
        <div class="col-lg-1 py-3 py-md-2 bg-warning">
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <i class="fa fa-money" style="font-size: 95px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center">
            <div class="d-flex justify-content-center mb-1">
              <h4 class="mb-0 text-dark">ราคาเรียน</h4>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2 text-dark">15,000</span>
              <span class="h5 mb-0">.-</span>
            </div>
          </div>
        </div>
        <div class="col-lg-1 py-3 py-md-2 bg-warning">
        </div>
      </div>
      <div class="col-md-12 text-center" style="margin-top: 50px;">
        <img src="{{ asset('img/blog/thumb-4.jpg') }}" alt="Image" class="img-fluid">
        <button class="btn btn-primary mt-3 js-pricing-submit-button" style="width: 100%;">Get started</button>
      </div>
    </div>
  </section>
  <section class="bg-light pt-5 pb-5">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
          <div class="card card-body align-items-center shadow" style="height: 400px;">
            <div class="text-center mb-4">
              <h2>ประโยชน์ที่จะได้รับ</h2>
            </div>
            <ul class="list-unstyled p-0">
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3" style="line-height:30px;">เข้าใจพื้นฐานการทำ Data Analytics</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">รู้วิธีการจัดเตรียมและการ Cleaning data</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">เข้าใจการทำงานของ Power Query</h5>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0">
          <div class="card card-body align-items-center shadow" style="height: 400px;">
            <div class="text-center mb-4">
              <h2>กลุ่มเป้าหมาย</h2>
            </div>
            <ul class="list-unstyled p-0">
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3" style="line-height:30px;">Data Analyst</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">Data Science</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">Data Engineer</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">ผู้สนใจทั่วไป</h5>
                </div>
              </li>
            </ul>
          </div>
        </div><div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
          <div class="card card-body align-items-center shadow" style="height: 400px;">
            <div class="text-center mb-4">
              <h2>พื้นฐานของผู้เข้าอบรม</h2>
            </div>
            <ul class="list-unstyled p-0">
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3" style="line-height:30px;">พื้นฐานการใช้งานระบบปฎิบัติการ Windows</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">มีพื้นฐานการจัดการ Database เบื้องต้น</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">เข้าใจการเขียน SQL เบื้องต้น</h5>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-5 pb-5">
    <div class="container">
      <div class="mt-5">
        <div class="pricing-table-section text-center text-lg-left">
          <div class="row no-gutters">
            <div class="col">
              <h5 class="mb-4">Basic Features</h5>
            </div>
          </div>
          <div class="border rounded">
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">Introduction</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> What is Data analysis</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Data science vs Data Analytics</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Power platform</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Power BI service</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Power BI desktop</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">1. Building first report</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Importing and transforming data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="#" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create data model</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create report</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Publishing your report</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create dashboard</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">2. Importing data</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Direct query</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Privacy levels for data source</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Importing Excel data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Importing Web data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">3. Preparing the data</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Introduction to Power Query</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Add and remove steps</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>What is M code</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create Power BI Data flow</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Custom data connector</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Error handling</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Adding python to Power BI</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">4. Model the Data</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Design data model</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Shaping data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Combine queries</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Add custom columns</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Duplicate and Referencing data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using unpivot</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using a custom functions</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">5. Relationships and Hierarchies</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Understanding relationships</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Manage relationships</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using relationship editor</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Resolving issues with Bridge table</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Working with hierarchy</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Add permanent hierarchy</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">6. Using Data Analytic Expression (DAX)</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Adding business logic</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Calculated columns</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Measures</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using CALCULATE</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using filter</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using ALL</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Iterators</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Evaluation contexts</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">7. Working with Date and Time</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using functions for date and time</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create calculated column for date table</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Working with date hierarchies</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using time intelligences</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">8. Reporting</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create table</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create matrix</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create line and bar chart</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create stacked chart</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create pie and donut chart</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create funnel chart</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create waterfall chart</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create tree map</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">9. Handling Unusual Data</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Display missing category data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Display geographical data</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Integrate custom validation</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">10. Interactivity in reports</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Slicer</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>What-if parameters</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Grouping and binding</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">11. Create and custom dashboard</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create first dashboard</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Report vs dashboard</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Customize existing report</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">12. Building content</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Dashboard tile</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tile caching</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tile configuration options</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tile navigation and refresh time</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Building mobile dashboard</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Medica tile</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">13. Theme and style</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Theme behavior</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Overriding theme color</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Overriding visual settings</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Additional theme options</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">14. Managing dashboard</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Updating dashboard tile</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Duplicate dashboards</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Observing change</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Handling changes with versioning</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">15. Securing Access to Content</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Power BI Security</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Using groups</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Sharing content</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Granting and denying access</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Implement multi-factor authentication</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">16. App and App Workspace</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create app workspace</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Give and revoke privileges</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create app</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">17. Row level security</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span> Roles and rules</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Validating row level security</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Limitation</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Promote and certify dataset</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Configure incremental refresh settings</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop

