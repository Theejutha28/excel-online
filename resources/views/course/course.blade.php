@extends('layouts.default')
@section('content')
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-4 mb-3 mb-md-4">
        <div class="card h-100 hover-box-shadow">
          <img class="card-img-top hover-fade-out" src="{{ asset('img/course/MasterExcel.png') }}" alt="blog.5.image">
          <div class="card-body">
            <h3>Master Excel</h3>
          </div>
          <div class="card-footer d-flex justify-content-between align-items-center">
            <a href="{{ route('master-excel') }}" class="badge badge-pill badge-info" style="width: 100%;">Read More</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-3 mb-md-4">
        <div class="card h-100 hover-box-shadow">
          <img class="card-img-top hover-fade-out" src="{{ asset('img/course/excel function.png') }}" alt="blog.5.image">
          <div class="card-body">
            <h3>Formulas and Functions</h3>
          </div>
          <div class="card-footer d-flex justify-content-between align-items-center">
            <a href="{{ route('formular-and-function') }}" class="badge badge-pill badge-info" style="width: 100%;">Read More</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-3 mb-md-4">
        <div class="card h-100 hover-box-shadow">
          <img class="card-img-top hover-fade-out" src="{{ asset('img/course/ExcelDataVisualization.png') }}" alt="blog.5.image">
          <div class="card-body">
            <h3>Excel Data Visualization</h3>
          </div>
          <div class="card-footer d-flex justify-content-between align-items-center">
            <a href="{{ route('excel-data-visualization') }}" class="badge badge-pill badge-info" style="width: 100%;">Read More</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-3 mb-md-4">
        <div class="card h-100 hover-box-shadow">
          <img class="card-img-top hover-fade-out" src="{{ asset('img/course/MarcosandVBA.png') }}" alt="blog.5.image">
          <div class="card-body">
            <h3>Macros and VBA</h3>
          </div>
          <div class="card-footer d-flex justify-content-between align-items-center">
            <a href="{{ route('macros-and-VBA') }}" class="badge badge-pill badge-info" style="width: 100%;">Read More</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 mb-3 mb-md-4">
        <div class="card h-100 hover-box-shadow">
          <img class="card-img-top hover-fade-out" src="{{ asset('img/course/PowerBI.png') }}" alt="blog.5.image">
          <div class="card-body">
            <h3>Power BI</h3>
          </div>
          <div class="card-footer d-flex justify-content-between align-items-center">
            <a href="{{ route('power-BI') }}" class="badge badge-pill badge-info" style="width: 100%;">Read More</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

