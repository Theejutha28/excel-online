@extends('layouts.default')
@section('script')
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
@stop
@section('content')
  <section class="pt-5 pb-5">
    <div class="container">
      <div class="row justify-content-end bg-white display-none-dt">
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <i class='fa fa-clock' style="font-size: 70px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <!-- <i class='fa fa-clock' style="font-size: 100px;"></i> -->
              <i class="far fa-money-bill-alt" style="font-size: 70px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <h4 class="mb-0">ระยะเวลาเรียน</h4>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2">9</span>
              <span class="h5 mb-0">ชั่วโมง</span>
            </div>
          </div>
        </div>
        
        <div class="col-lg-6 col-6 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center">
            <div class="d-flex justify-content-center mb-1">
              <h4 class="mb-0 text-dark">ราคาเรียน</h4>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2 text-dark">2,900</span>
              <span class="h5 mb-0">.-</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-end bg-white display-none-mb">
        <div class="col-lg-1 py-3 py-md-2 bg-info">
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-white">
            <div class="d-flex mb-1">
              <i class='fa fa-clock' style="font-size: 100px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-info">
          <div class="d-flex flex-column align-items-center text-white">
            <div class="d-flex mb-1">
              <h4 class="mb-0">ระยะเวลาเรียน</h4>
            </div>
            <div class="d-flex align-items-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2">9</span>
              <span class="h5 mb-0">ชั่วโมง</span>
            </div>
          </div>
        </div>
        <div class="col-lg-1 py-3 py-md-2 bg-info">
        </div>
        <div class="col-lg-1 py-3 py-md-2 bg-warning">
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center text-white">
            <div class="d-flex justify-content-center mb-1">
              <i class="far fa-money-bill-alt" style="font-size: 95px;"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-2 py-3 py-md-2 bg-warning">
          <div class="d-flex flex-column align-items-center text-center">
            <div class="d-flex justify-content-center mb-1">
              <h4 class="mb-0 text-dark">ราคาเรียน</h4>
            </div>
            <div class="d-flex align-items-center justify-content-center mb-1">
              <span class="display-4 mb-0 mr-1 mr-sm-2 text-dark">2,900</span>
              <span class="h5 mb-0">.-</span>
            </div>
          </div>
        </div>
        <div class="col-lg-1 py-3 py-md-2 bg-warning">
        </div>
      </div>
      <div class="col-md-12 text-center" style="margin-top: 50px;">
        <img src="{{ asset('img/blog/thumb-4.jpg') }}" alt="Image" class="img-fluid">
        <button class="btn btn-primary mt-3 js-pricing-submit-button" style="width: 100%;">Get started</button>
      </div>
    </div>
  </section>
  <section class="bg-light pt-5 pb-5">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
          <div class="card card-body align-items-center shadow" style="height: 400px;">
            <div class="text-center mb-4">
              <h2>วัตถุประสงค์</h2>
            </div>
            <ul class="list-unstyled p-0">
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3" style="line-height:30px;">ผู้อบรมมีความเข้าใจหลักการเขียนโปรแกรม เพื่อสั่งงานคอมพิวเตอร์ได้</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">ผู้อบรมมีความเข้าใจหลักการพัฒนาซอฟต์แวร์ได้</h5>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0">
          <div class="card card-body align-items-center shadow" style="height: 400px;">
            <div class="text-center mb-4">
              <h2>เหมาะสำหรับ</h2>
            </div>
            <ul class="list-unstyled p-0">
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3" style="line-height:30px;">ผู้ที่ต้องการเรียนรู้หลักการเขียนโปรแกรมคอมพิวเตอร์ด้วยภาษา Python</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">ผู้ที่ต้องการเรียนรู้ภาษา Python เพื่อต่อยอดในงานทางด้าน Data science ในอนาคต</h5>
                </div>
              </li>
            </ul>
          </div>
        </div><div class="col-sm-9 col-md-7 col-lg-4 mb-3 mb-md-4 mb-lg-0" data-aos="fade-up" data-aos-delay="100">
          <div class="card card-body align-items-center shadow" style="height: 400px;">
            <div class="text-center mb-4">
              <h2>พื้นฐานของผู้เข้าอบรม</h2>
            </div>
            <ul class="list-unstyled p-0">
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3" style="line-height:30px;">ผู้เข้าอบรมสามารถใช้งานคอมพิวเตอร์พื้นฐานได้ดี</h5>
                </div>
              </li>
              <li class="my-3">
                <div class="d-flex align-items-center">
                  <div class="rounded-circle bg-success-alt">
                    <img src="{{ asset('img/icons/interface/icon-check.svg') }}" alt=" icon=" class="m-2 icon icon-sm">
                  </div>
                  <h5 class="mb-0 ml-3"  style="line-height:30px;">มีความตั้งใจและอยากเรียนรู้</h5>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="pt-5 pb-5">
    <div class="container">
      <div class="mt-5">
        <div class="pricing-table-section text-center text-lg-left">
          <div class="row no-gutters">
            <div class="col">
              <h5 class="mb-4">Basic Features</h5>
            </div>
          </div>
          <div class="border rounded">
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">Introduction</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the most powerful formulas and functions in Excel (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">1. Formula and Function Tips and Shortcuts</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Display and highlight formulas (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="#" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the auditing tools (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use entire row/column references (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use entire row/column references (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Change formulas to values and update values without formulas (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Simplify debugging formulas with the F9 key (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Enhance readability with range names (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create 3D formulas to tabulate data from multiple sheets (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">2. IF and Related Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Explore IF logical tests and use relational operators (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create and expand the use of nested IF statements (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Create compound logical tests with AND, OR, NOT, and IF (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use IFS for multiple conditions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">3. Lookup and Reference Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Explore the VLOOKUP and HLOOKUP functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Find approximate matches with VLOOKUP and HLOOKUP (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use VLOOKUP to find exact matches and search large tables (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Find table-like data within a function using CHOOSE (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the SWITCH function for formula-embedded selection (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Locate data with the MATCH function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Retrieve information by location with the INDEX function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the MATCH and INDEX functions together (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Document formulas with the FORMULATEXT function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Extract and count unique entries from a list with UNIQUE (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the XLOOKUP function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">4. Power Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tabulate data using a single criterion with COUNTIF, SUMIF, and AVERAGEIF (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tabulate data using multiple criteria with COUNTIFS, SUMIFS, and AVERAGEIFS (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use MAXIFS and MINIFS (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-lock"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the SUBTOTAL function to prevent double counting (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">5. Statistical Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Find middle and most common values with MEDIAN and MODE (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Rank data without sorting using RANK and RANK.EQ (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Find the largest and smallest values with LARGE and SMALL (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tabulate blank cells with the COUNTBLANK function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use COUNT, COUNTA, and the status bar (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 10px !important; padding-top: 10px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">6. Math Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Work with the ROUND, ROUNDUP, and ROUNDDOWN functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use MROUND, CEILING, and FLOOR for specialized rounding (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use INT, TRUNC, ODD, and EVEN for specialized rounding (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use MOD to find remainders and apply conditional formatting (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Explore practical uses for RAND, RANDARRAY, and RANDBETWEEN (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Convert a value between measurement systems with CONVERT (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the AGGREGATE function to bypass errors and hidden data (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use ROMAN and ARABIC to display different number systems (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">7. Date and Time Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Understand Excel date and time capabilities in formulas (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use various date and time functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the TODAY and NOW functions for date and time entry (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Identify weekdays with the WEEKDAY function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Count working days and completion dates (NETWORKDAYS and WORKDAY) (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Tabulate date differences with the DATEDIF function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Calculate dates with EDATE and EOMONTH (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">8. Reference Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Get data from remote cells with the OFFSET function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Return references with the INDIRECT function (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use INDIRECT with Data Validation for multitiered pick lists (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">9. Text Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Locate and extract data with FIND, SEARCH, and MID (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Extract data with the LEFT and RIGHT functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use the TRIM function to remove unwanted spaces in a cell (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Combine data with symbols (&) and CONCATENATE (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use CONCAT and TEXTJOIN to combine data from different cells (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Adjust alphabetic case with UPPER, LOWER, and PROPER (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Adjust character content with REPLACE and SUBSTITUTE (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use utility text functions: TEXT, REPT, VALUE, and LEN (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">10. Information Functions</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Extract information with the CELL and INFO functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Explore various information functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Use several error-checking functions (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Track and highlight formula cells with ISFORMULA (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-12 col-lg-12  py-3" style="padding-bottom: 15px !important; padding-top:15px !important; border-bottom: 0px solid #EAEDF2;">
                <div class="d-flex align-items-center justify-content-center justify-content-lg-start">
                  <h6 class="mb-0 ml-lg-4">Conclusion</h6>
                </div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-8 col-lg-10 d-flex py-3" style="padding-bottom: 5px !important; padding-top: 5px !important; padding-left: 25px; text-align: left;">
                <span>Next steps (38 วินาที)</span>
              </div>
              <div class="col-4 col-lg-2 d-flex justify-content-center py-3" style="padding-bottom: 5px !important; padding-top: 5px !important;">
                <a data-fancybox href="https://vimeo.com/166034462#t=32s" class="btn btn-lg btn-dark rounded-circle" style="width: 45px; height: 45px;">
                  <i class="fa fa-play"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop

