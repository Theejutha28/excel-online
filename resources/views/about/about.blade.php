@extends('layouts.default')
@section('content')
    <section class="row no-gutters p-0 border-top border-bottom">
      	<div class="col-lg-6">
	        <section>
	          <div class="container">
	            <div class="row justify-content-center">
	              <div class="col col-md-10 col-xl-9 text-center text-lg-left">
	                <h1 class="display-3">Contact Us</h1>
	                <address class="lead mb-5">
	                  <p>443 Park Avenue South,</p>
	                  <p>Kips Bay</p>
	                  <p>New York City</p>

	                </address>
	                <div class="d-flex flex-column justify-content-center justify-content-lg-start">
	                  <div class="mx-3 mx-lg-0 mr-lg-5 mb-4">
	                    <h5>Email us</h5>
	                    <a href="#" class="lead">hello@jumpstart.io</a>
	                  </div>
	                  <div class="mx-3 mx-lg-0 mr-lg-5 mb-4">
	                    <h5>Call any time</h5>
	                    <a href="#" class="lead">1800 488 328</a>
	                  </div>
	                  <div class="mx-3 mx-lg-0 mr-lg-5 mb-4">
	                    <h5>Get in touch</h5>
	                    <a href="#" class="lead">Leave a message</a>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </section>
      	</div>
      	<div class="col-lg-6">
        
      	</div>
    </section>
@stop