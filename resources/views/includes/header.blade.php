<div class="navbar-container bg-primary-3">
  <nav class="navbar navbar-expand-lg navbar-dark" data-sticky="top">
    <div class="container">
      @include('includes.header-item')
    </div>
  </nav>
</div>