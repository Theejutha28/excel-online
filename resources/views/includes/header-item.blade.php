<a class="navbar-brand navbar-brand-dynamic-color fade-page" href="index.html">
  Excel Online
</a>
<div class="d-flex align-items-center order-lg-3">
  <a href="#" class="btn btn-primary ml-lg-4 mr-3 mr-md-4 mr-lg-0 d-none d-sm-block order-lg-3">เรียนออนไลน์</a>
  <button aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target=".navbar-collapse" data-toggle="collapse" type="button">
    <img alt="Navbar Toggler Open Icon" class="navbar-toggler-open icon icon-sm" src="{{ asset('img/icons/interface/icon-menu.svg') }}">
    <img alt="Navbar Toggler Close Icon" class="navbar-toggler-close icon icon-sm" src="{{ asset('img/icons/interface/icon-x.svg') }}">
  </button>
</div>
<div class="collapse navbar-collapse order-3 order-lg-2 justify-content-lg-end" id="navigation-menu">
  <ul class="navbar-nav my-3 my-lg-0">
    <li class="nav-item">
      <div class="dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item" href="{{ route('home') }}" role="button">หน้าหลัก</a>
      </div>
    </li>
    <li class="nav-item">
      <div class="dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item" href="{{ route('excelfuntion') }}" role="button">สูตร Excel</a>
      </div>
    </li>
    <li class="nav-item">
      <div class="dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item" href="{{ route('blog') }}" role="button">Blog</a>
      </div>
    </li>
    <li class="nav-item">
      <div class="dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item" href="{{ route('course') }}" role="button">หลักสูตร</a>
      </div>
    </li>
    <li class="nav-item">
      <div class="dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item" href="{{ route('course-online') }}" role="button">คอร์สออนไลน์</a>
      </div>
    </li>
    <li class="nav-item">
      <div class="dropdown">
        <a aria-expanded="false" aria-haspopup="true" class="nav-link nav-item" href="{{ route('about') }}" role="button">ติดต่อเรา</a>
      </div>
    </li>
  </ul>
</div>