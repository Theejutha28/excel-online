<!-- Begin loading animation -->
<link rel="stylesheet" href="{{ asset('css/loaders/loader-pulse.css') }}" />
<!-- End loading animation -->
<link rel="stylesheet" href="{{ asset('css/theme.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
<link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree&display=swap" rel="stylesheet">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/aos.js') }}"></script>
<script src="{{ asset('js/clipboard.min.js') }}"></script>
<script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('js/flatpickr.min.js') }}"></script>
<script src="{{ asset('js/flickity.pkgd.min.js') }}"></script>
<script src="{{ asset('js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('js/jarallax.min.js') }}"></script>
<script src="{{ asset('js/jarallax-video.min.js') }}"></script>
<script src="{{ asset('js/jarallax-element.min.js') }}"></script>
<script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('js/jquery.smartWizard.min.js') }}"></script>
<script src="{{ asset('js/plyr.polyfilled.min.js') }}"></script>
<script src="{{ asset('js/prism.js') }}"></script>
<script src="{{ asset('js/scrollMonitor.js') }}"></script>
<script src="{{ asset('js/smooth-scroll.polyfills.min.js') }}"></script>
<script src="{{ asset('js/svg-injector.umd.production.js') }}"></script>
<script src="{{ asset('js/twitterFetcher_min.js') }}"></script>
<script src="{{ asset('js/typed.min.js') }}"></script>
<script src="{{ asset('js/theme.js') }}"></script>
<script type="text/javascript">
  window.addEventListener("load",function(){document.querySelector('body').classList.add('loaded');});
</script>