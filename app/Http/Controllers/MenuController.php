<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{

  public function menulist() {
    
    $menus = Menu::all();
    // dd(Menu::all());
    return view('home.home')->with([
      'menus' => $menus
    ]);

  }

}
