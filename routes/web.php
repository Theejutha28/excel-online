<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MenuController@menulist')->name('home');
Route::get('/ติดต่อเรา', function () { return view('about/about'); })->name('about');
Route::get('/Blog', function () { return view('blog/blog'); })->name('blog');
Route::get('/สูตรExcel', function () { return view('excelfuntion/excelfuntion'); })->name('excelfuntion');

Route::get('/course-online', function () { return view('course-online/course-online'); })->name('course-online');

Route::get('/course', function () { return view('course/course'); })->name('course');
Route::get('/course/master-excel', function () { return view('course/master-excel'); })->name('master-excel');
Route::get('/course/formular-and-function', function () { return view('course/formular-and-function'); })->name('formular-and-function');
Route::get('/course/excel-data-visualization', function () { return view('course/excel-data-visualization'); })->name('excel-data-visualization');
Route::get('/course/macros-and-VBA', function () { return view('course/macros-and-VBA'); })->name('macros-and-VBA');
Route::get('/course/power-BI', function () { return view('course/power-BI'); })->name('power-BI');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
